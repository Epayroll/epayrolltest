describe('Employer Home ', function () {
  
    var trading_name = ["Marist Youth Care Limited Test","Marist Youth Care Limited","Marist Youth Care","Marist Youth Limited"];
    var x = Math.floor((Math.random() * 3) + 1);

    var tradingname = trading_name[x];

    it ('Can Change the employer Information (Trading Name)', function(){
        cy.visit('https://saasuat.epayroll.com.au')
        cy.get('#UserName').type('support@epayroll.com.au')
        cy.get('#Login > div.form-actions > button').click()
        cy.get('#Password').type('WinterIsComing')
        cy.get('#Login > div.form-actions > button.submit.btn.btn-primary.pull-right').click()

        cy.xpath('//header/div[1]/ul[2]/li[2]/a[1]').click()
        cy.xpath('//a[contains(@href,"ChangeEmployer.aspx")]').click()
        cy.get('#phMain_ddlEmployers').select('1837')
        cy.xpath('//*[@id="nav"]/li[4]/ul/li[6]/a').click()
        cy.visit('https://saasuat.epayroll.com.au/employer/default.aspx')
        cy.get('#phMain_butEdit').click()
        cy.get('#phMain_editEmployer_txtTradingName').clear()
        cy.wait(2000)
        cy.get('#phMain_editEmployer_txtTradingName').type(tradingname)
        cy.get('#phMain_editEmployer_butSave').click()

        cy.get('#content > div > div.row > div > div:nth-child(1) > div.widget-content.form-horizontal.row-border > div:nth-child(1) > div:nth-child(4)').should("contain.text",tradingname)
    })

    it ('Can Change the employer Information (Bank Details)', function(){
        cy.clearCookies()
        cy.visit('https://saasuat.epayroll.com.au')
        cy.get('#UserName').type('support@epayroll.com.au')
        cy.get('#Login > div.form-actions > button').click()
        cy.get('#Password').type('WinterIsComing')
        cy.get('#Login > div.form-actions > button.submit.btn.btn-primary.pull-right').click()

        cy.xpath('//header/div[1]/ul[2]/li[2]/a[1]').click()
        cy.xpath('//a[contains(@href,"ChangeEmployer.aspx")]').click()
        cy.get('#phMain_ddlEmployers').select('1837')
        cy.xpath('//*[@id="nav"]/li[4]/ul/li[6]/a').click()
        cy.visit('https://saasuat.epayroll.com.au/employer/default.aspx')
        cy.get('#phMain_butEdit').click()
        cy.get('#phMain_editEmployer_txtAccountName').clear()
        cy.wait(2000)
        cy.get('#phMain_editEmployer_txtAccountName').type(tradingname)
        cy.get('#phMain_editEmployer_butSave').click()

        cy.get('#content > div > div.row > div > div:nth-child(5) > div.widget-content.form-horizontal.row-border > div:nth-child(1) > div:nth-child(2)').should("contain.text",tradingname)
    })
})

describe('Employer Paycode ', function () {
  
    var Paycode_description = ["Workers Compensation","Workers Return Compensation","Workers Test Compensation"];
    var x = Math.floor((Math.random() * 2) + 1);

    var paycode_desc = Paycode_description[x];

    it ('Can add a paycode', function(){
        cy.clearCookies()
        cy.visit('https://saasuat.epayroll.com.au')
        cy.get('#UserName').type('support@epayroll.com.au')
        cy.get('#Login > div.form-actions > button').click()
        cy.get('#Password').type('WinterIsComing')
        cy.get('#Login > div.form-actions > button.submit.btn.btn-primary.pull-right').click()

        cy.xpath('//header/div[1]/ul[2]/li[2]/a[1]').click()
        cy.xpath('//a[contains(@href,"ChangeEmployer.aspx")]').click()
        cy.get('#phMain_ddlEmployers').select('1837')
        cy.xpath('//*[@id="nav"]/li[4]/ul/li[6]/a').click()
        cy.xpath('//a[contains(@href,"/employer/paycode/employerpaycode.aspx")]').click()
        cy.get('#ctl00_phMain_ddlPaycodeTemplate_Input').click()
        cy.xpath('//li[contains(text(),"Workers Comp RTW")]').click()
        cy.get('#phMain_butAdd').click()
        cy.get('#ctl00_phMain_rgPaycodes_ctl00__114 > td:nth-child(3)').should("contain.text",'Workers Comp RTW')
    })

    it ('Can edit a paycode', function(){
        cy.clearCookies()
        cy.visit('https://saasuat.epayroll.com.au')
        cy.get('#UserName').type('support@epayroll.com.au')
        cy.get('#Login > div.form-actions > button').click()
        cy.get('#Password').type('WinterIsComing')
        cy.get('#Login > div.form-actions > button.submit.btn.btn-primary.pull-right').click()

        cy.xpath('//header/div[1]/ul[2]/li[2]/a[1]').click()
        cy.xpath('//a[contains(@href,"ChangeEmployer.aspx")]').click()
        cy.get('#phMain_ddlEmployers').select('1837')
        cy.xpath('//*[@id="nav"]/li[4]/ul/li[6]/a').click()
        cy.xpath('//a[contains(@href,"/employer/paycode/employerpaycode.aspx")]').click()
        cy.get('#ctl00_phMain_rgPaycodes_ctl00_ctl232_lnkEdit').click()
        cy.get('#ctl00_phMain_rgPaycodes_ctl00_ctl233_txtPaycodeDescription').clear()
        cy.wait(2000)
        cy.get('#ctl00_phMain_rgPaycodes_ctl00_ctl233_txtPaycodeDescription').type(paycode_desc)
        cy.get('#ctl00_phMain_rgPaycodes_ctl00_ctl233_butSaveChanges').click()
        cy.get('#ctl00_phMain_rgPaycodes_ctl00__114 > td:nth-child(4)').should("contain.text",paycode_desc)
    })

    it ('Can delete a paycode', function(){
        cy.clearCookies()
        cy.visit('https://saasuat.epayroll.com.au')
        cy.get('#UserName').type('support@epayroll.com.au')
        cy.get('#Login > div.form-actions > button').click()
        cy.get('#Password').type('WinterIsComing')
        cy.get('#Login > div.form-actions > button.submit.btn.btn-primary.pull-right').click()

        cy.xpath('//header/div[1]/ul[2]/li[2]/a[1]').click()
        cy.xpath('//a[contains(@href,"ChangeEmployer.aspx")]').click()
        cy.get('#phMain_ddlEmployers').select('1837')
        cy.xpath('//*[@id="nav"]/li[4]/ul/li[6]/a').click()
        cy.xpath('//a[contains(@href,"/employer/paycode/employerpaycode.aspx")]').click()
        cy.get('#ctl00_phMain_rgPaycodes_ctl00_ctl232_lnkDelete').click()
        
    })

   
})

describe('Employer Superfund', function () {


    it ('Can add a superfund', function(){
        cy.clearCookies()
        cy.visit('https://saasuat.epayroll.com.au')
        cy.get('#UserName').type('support@epayroll.com.au')
        cy.get('#Login > div.form-actions > button').click()
        cy.get('#Password').type('WinterIsComing')
        cy.get('#Login > div.form-actions > button.submit.btn.btn-primary.pull-right').click()
 
        cy.xpath('//header/div[1]/ul[2]/li[2]/a[1]').click()
        cy.xpath('//a[contains(@href,"ChangeEmployer.aspx")]').click()
        cy.get('#phMain_ddlEmployers').select('1837')
        cy.xpath('//*[@id="nav"]/li[4]/ul/li[6]/a').click()
        cy.visit('https://saasuat.epayroll.com.au/superfund/selectedfund.aspx')
        cy.visit('https://saasuat.epayroll.com.au/superfund/NewFund.aspx')
        cy.get('#phMain_dg_butSelect_0').click()
        cy.get('#phMain_butSave').click()
        
    })


    it ('Can add a self-managed superfund', function(){
        cy.clearCookies()
        cy.visit('https://saasuat.epayroll.com.au')
        cy.get('#UserName').type('support@epayroll.com.au')
        cy.get('#Login > div.form-actions > button').click()
        cy.get('#Password').type('WinterIsComing')
        cy.get('#Login > div.form-actions > button.submit.btn.btn-primary.pull-right').click()
        cy.xpath('//header/div[1]/ul[2]/li[2]/a[1]').click()
        cy.xpath('//a[contains(@href,"ChangeEmployer.aspx")]').click()
        cy.get('#phMain_ddlEmployers').select('1837')
        cy.xpath('//*[@id="nav"]/li[4]/ul/li[6]/a').click()
        cy.visit('https://saasuat.epayroll.com.au/superfund/selectedfund.aspx')
        cy.visit('https://saasuat.epayroll.com.au/superfund/EditSMFund.aspx?action=add')
        cy.get('#phMain_txtProductName').type(Name_fund())
        function Name_fund() {
            var text = "";
            var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        
            for (var i = 0; i < 6; i++)
              text += possible.charAt(Math.floor(Math.random() * possible.length));
        
            return text;
          }

        cy.get('#ctl00_phMain_txtABN').type('39001987739')
        cy.get('#ctl00_phMain_txtBSB').type('062191')
        cy.get('#ctl00_phMain_txtAccountNumber').type('123456789')
        cy.get('#phMain_ddlESA').select('2')
        cy.wait(2000)
        cy.get('#phMain_butSave').click()
    })

  
})