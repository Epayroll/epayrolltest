describe('Leave and Expense Application ', function () {
    
    it ('Can apply for a leave application', function(){
        cy.visit('https://saasuat.epayroll.com.au')
        cy.get('#UserName').type('mohammadarafat@epayroll.com')
        cy.get('#Login > div.form-actions > button').click()
        cy.get('#Password').type('abcde12345')
        cy.get('#Login > div.form-actions > button.submit.btn.btn-primary.pull-right').click()

        cy.xpath('//*[@id="nav"]/li[4]/ul/li[3]/a').click()
        cy.visit('https://saasuat.epayroll.com.au/lema/default.aspx')
        cy.xpath('//button[@id="phMain_ctl02_butLeaveApply"]').click()
        cy.xpath('//a[@id="phMain_butSubmit"]').click()
        cy.xpath('//button[@id="phMain_ctl02_butLeaveApply"]').should("contain.text",'Apply for Leave')
    })

    it ('Can apply for a expense application with custom amount', function(){
        cy.visit('https://saasuat.epayroll.com.au')
        cy.get('#UserName').type('mohammadarafat@epayroll.com')
        cy.get('#Login > div.form-actions > button').click()
        cy.get('#Password').type('abcde12345')
        cy.get('#Login > div.form-actions > button.submit.btn.btn-primary.pull-right').click()

        cy.xpath('//*[@id="nav"]/li[4]/ul/li[3]/a').click()
        cy.visit('https://saasuat.epayroll.com.au/lema/default.aspx')
        cy.xpath('//button[@id="phMain_ctl03_butExpenseApply"]').click()
        cy.xpath('//input[@id="ctl00_phMain_txtAmount"]').type('450')
        cy.xpath('//input[@id="phMain_butSubmit"]').click()
        cy.xpath('//button[@id="phMain_ctl03_butExpenseApply"]').should("contain.text",'Apply for an Expense Reimbursement')
    })
})
