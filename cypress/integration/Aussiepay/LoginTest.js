/// <reference types="Cypress" />

import LoginPage from '../PageObject/Loginpage'


describe  ('User Can Login To System' , function() {

    it('User can login with valid credentials', function() {
    
        const login= new LoginPage()
        login.visit()
        login.fillEmail('mohammadarafat@epayroll.com')
        login.nextButton()
        login.fillPassword('abcde12345')
        login.sumbit()
        
        // Assertion 
        cy.title().should('be.equal', 'Aussiepay')  

        /* Checking the Home Component */

        cy.xpath('//li[@class="newdashboard module"]/a/p').then(function(textelement)
        {
            expect(textelement.text()).to.include('Home')            
        })

        /* Checking the Correct Footer */

        cy.xpath('//div[@class="row"]/div/footer/p').then(function(textelement)
        {
            expect(textelement.text()).to.include('v12.20.2')            
        })

    })

    it('Users can not login with invalid credentials', function() {
    
        const login= new LoginPage()
        login.visit()
        login.fillEmail('mohammadarafatsa@epayroll.com')
        login.nextButton()
        login.fillPassword('abcde12345')
        login.sumbit()

        // Assertion 

        /* Checking the Home Component */
        cy.xpath('//div[@class="alert alert-danger"]').then(function(textelement)
        {
            expect(textelement.text()).to.include('Login failed.')            
        })
    })

    it('Users can not reset password with wrong email', function() {
    
        const login= new LoginPage()
        login.visit()
        login.fillEmail('mohammadarafatsa@epayroll.com')
        login.nextButton()
        login.fillPassword('abcde12345')
        login.forgetPassword()

        // Assertion 

        /* Checking the Home Component */
        cy.xpath('//div[@class="alert alert-danger"]').then(function(textelement)
        {
            expect(textelement.text()).to.include('Please enter a valid email address.')
        })
    })

    
})