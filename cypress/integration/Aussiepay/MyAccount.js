import LoginPage from '../PageObject/Loginpage'
import MyAccount from '../PageObject/MyAccount'


var streetName = ["19/20 Railway Street", "456 George St" , "56 Delhi Road" , "25 Bigge St", "26 Paramatta Road"];
var x = Math.floor((Math.random() * 4) + 1);
const _streetname = streetName[x];

describe('MyAccount Home', function () {

     it ('Can navigate to the correct screen for General Information', function(){
        const login= new LoginPage()
        login.visit()
        login.fillEmail('mohammadarafat@epayroll.com')
        login.nextButton()
        login.fillPassword('abcde12345')
        login.sumbit()
 
        const myaccount = new MyAccount()
        myaccount.NavigateToMyAccountHome()
        
        //Assert
        cy.xpath('//*[@id="content"]/div/div[3]/div/div[1]/div/div/div[2]/div[1]/div[2]/div/div[2]/p').should("contain.text",'mohammadarafat@epayroll.com')       
    })   

    it ('Can edit general Information', function(){
        const login= new LoginPage()
        login.visit()
        login.fillEmail('mohammadarafat@epayroll.com')
        login.nextButton()
        login.fillPassword('abcde12345')
        login.sumbit()
 
        const myaccount = new MyAccount()
        myaccount.NavigateToMyAccountHome()
        myaccount.EditGeneralInformation()
        myaccount.RetypeGI(_streetname)
        myaccount.SaveGI()
        
        //Assert
        cy.xpath('//*[@id="content"]/div/div[3]/div/div[1]/div/div/div[2]/div[1]/div[3]/div/div[2]/p').should("contain.text",_streetname)       
    }) 

    it ('Can change password', function(){
        const login= new LoginPage()
        login.visit()
        login.fillEmail('mohammadarafat@epayroll.com')
        login.nextButton()
        login.fillPassword('abcde12345')
        login.sumbit()
 
        const myaccount = new MyAccount()
        myaccount.NavigateToChangePassword()
        myaccount.TypeOldPassword('abcde12345')
        myaccount.TypeNewPassword('abcde12345')
        myaccount.SavePassword()
        
        //Assert
        cy.title().should('be.equal', 'Aussiepay')   
    })  
  
})