import LoginPage from '../PageObject/Loginpage'
import OnboardingPage from '../PageObject/OnboardingPage'

var files = ["Fair-Work-Information-Statement.pdf","Fair-Work-Information-Sheet.pdf", "Fair-Work-Information-Statement.pdf","Fair-Work-Information-Sheet.pdf" ];
var x = Math.floor((Math.random() * 3) + 1);

describe('Onboarding Test Cases : Upload Fairwork Document in Onboarding Wizard', function () {

   it ('Upload Fairwork Document in Onboarding Wizard', function(){
    const login= new LoginPage()
    login.visit()
    login.fillEmail('mohammadarafat@epayroll.com')
    login.nextButton()
    login.fillPassword('abcde12345')
    login.sumbit()            

    const onboard= new OnboardingPage()
    onboard.NavigateToOnboardingWizard()
    const fileName = files[x]
    cy.get('#selectfairworkAttachment').attachFile(fileName);
    cy.get('#btnSaveConfig').click()

    //Assertion

    cy.xpath('//*[@id="content"]/div/div[3]/div/div[2]/app-root/ng-component/ng-component/form/div/div[2]/div[7]/div[4]/div/span').should("contain.text",fileName)

    }) 

    it ('New Hire Start Date Approaching Notification', function(){
        const login= new LoginPage()
        login.visit()
        login.fillEmail('mohammadarafat@epayroll.com')
        login.nextButton()
        login.fillPassword('abcde12345')
        login.sumbit()            
    
        var value = x
        const onboard= new OnboardingPage()
        onboard.NavigateToOnboardingWizard()
        onboard.SetNumberOfDaysForR(value)
        cy.get('#btnSaveConfig').click()

        //Assertion 

        cy.xpath('//*[@id="content"]/div/div[3]/div/div[2]/app-root/ng-component/ng-component/form/div/div[2]/div[12]/div[2]/div[1]/div[2]/dx-number-box/div/div[1]/input').should('have.value', value)

    })


}) 


