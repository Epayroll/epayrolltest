import LoginPage from '../PageObject/Loginpage'
import OnboardingPage from '../PageObject/OnboardingPage'
        //--------------------------------------Test Data --------------------------------------------- 
        const serverId = 'jtp49cod';
        var familyname = ["Giroud", "Griezman" , "Messi" , "Zidan" , "Williams" , "Semedo", "Martial" , "Umtiti" , "Benzama" , "Aguero" , "Levandoski" , "Sterling" , "Ronalodo"];
        var givenname = ["Oliver", "Antonio" , "Leonel" , "Zinedin" , "Inaki" , "Nelson", "Antonio" , "Samuel" , "Karim" , "Kun" , "Robert", "Rahim" , "Chritiano"];
        var x = Math.floor((Math.random() * 12) + 1);


        var y = new Date();
        var num = y.getTime();
        var str_a = num.toString();
        var result = Number(str_a.slice(7, 12));        // this part is generating a random number 

        //---------------------------------------End----------------------------------------------------
        

        describe('Onboarding Test Cases : Initiate without contact', function () {

            var  _familyname = familyname[x];
            var _givenname = givenname[x];
            var _email = ((((_familyname.concat('.')).concat(_givenname)).concat('+')).concat(result)).concat('@aussiepay.com');

           it ('Can Initiate onboarding invitation for a new employee and also search and employee ', function(){
            const login= new LoginPage()
            login.visit()
            login.fillEmail('mohammadarafat@epayroll.com')
            login.nextButton()
            login.fillPassword('abcde12345')
            login.sumbit()            

            const onboard= new OnboardingPage()
            onboard.NavigateToMyEmployeeConfiguration()
            onboard.AddEmployee()
            onboard.ProvideEmployeeDetails(_familyname,_givenname,_email)
            onboard.SendInvitation()
            onboard.SearchEmployee(_email)

            //Assertion
            cy.xpath('//*[@id="tblEmployee"]/tbody/tr[1]/td[1]').should("contain.text",(_familyname.concat(', ')).concat(_givenname) )
        
           })    

           it('Users can proceed with invited onboarding link', () => {
            cy.mailosaurGetMessage(serverId, {
               sentTo: _email
            }).then(message => {
            var startonboarding = message.html.links[0].href;
   
            cy.visit(startonboarding)
            cy.title().should('be.equal', 'Welcome New Hire')
            const onboard= new OnboardingPage()
            onboard.ChangePassword()
         
                })
            })
        

            it( 'Finish Onboarding', () => {
                const login= new LoginPage()
                login.visit()
                login.fillEmail(_email)
                login.nextButton()
                login.fillPassword('abcde12345')
                login.sumbit()  

                const onboard= new OnboardingPage()
                onboard.NavigateToPersonalDetails()
                onboard. ProvidePersonalDetails() 
                onboard.ProvideEmmergencyDetails()
                onboard.ProvideTaxDetails()
                onboard.ProvideBankDetails()
                onboard.Finishonboard()


                //Assertion
                cy.xpath('//h2').should("contain.text","Onboarding Complete" )

            })


            it('Gets an onboarding finalization notificaiton', () => {
                cy.wait(5000)
                cy.mailosaurGetMessage(serverId, {
                   sentTo: 'Ehtel.BLAFIELD.142280@aussiepay.com.au'
                }).then(message => {
               expect(message.subject).to.equal(_givenname + ' '+ _familyname.toUpperCase() +' has completed onboarding with Employgroup Demo Account Managed');
             })
           })  

        
  
        
})    