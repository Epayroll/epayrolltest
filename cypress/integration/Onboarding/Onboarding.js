import LoginPage from '../PageObject/Loginpage'
import OnboardingPage from '../PageObject/OnboardingPage'
        //--------------------------------------Test Data --------------------------------------------- 
        const serverId = 'jtp49cod';
        var familyname = ["Giroud", "Griezman" , "Messi" , "Zidan" , "Williams" , "Semedo", "Martial" , "Umtiti" , "Benzama" , "Aguero" , "Levandoski" , "Sterling" , "Ronalodo"];
        var givenname = ["Oliver", "Antonio" , "Leonel" , "Zinedin" , "Inaki" , "Nelson", "Antonio" , "Samuel" , "Karim" , "Kun" , "Robert", "Rahim" , "Chritiano"];
        var x = Math.floor((Math.random() * 12) + 1);


        var y = new Date();
        var num = y.getTime();
        var str_a = num.toString();
        var result = Number(str_a.slice(7, 12));        // this part is generating a random number 

        //---------------------------------------End----------------------------------------------------
        

        describe('Onboarding Test Cases : Initiate without contact', function () {

            var  _familyname = familyname[x];
            var _givenname = givenname[x];
            var _email = ((((_familyname.concat('.')).concat(_givenname)).concat('+')).concat(result)).concat('@aussiepay.com');

           it ('Can Initiate onboarding invitation for a new employee and also search and employee ', function(){
            const login= new LoginPage()
            login.visit()
            login.fillEmail('mohammadarafat@epayroll.com')
            login.nextButton()
            login.fillPassword('abcde12345')
            login.sumbit()            

            const onboard= new OnboardingPage()
            onboard.NavigateToMyEmployeeConfiguration()
            onboard.AddEmployee()
            onboard.ProvideEmployeeDetails(_familyname,_givenname,_email)
            onboard.SendInvitation()
            onboard.SearchEmployee(_email)

            //Assertion
            cy.xpath('//*[@id="tblEmployee"]/tbody/tr[1]/td[1]').should("contain.text",(_familyname.concat(', ')).concat(_givenname) )
        
           })    

        it('Gets an onboarding email notificaiton', () => {
             cy.mailosaurGetMessage(serverId, {
                sentTo: _email
             }).then(message => {
            expect(message.subject).to.equal('Begin your Onboarding Process with Employgroup Demo Account Managed');
          })
        })   
  
        
})    
     
        describe('Onboarding Test Cases : Deleting a new hire', function () {

            var y = Math.floor((Math.random() * 11) + 1);
            var _familyname = familyname[y];
            var _givenname = givenname[y];
            var _email = ((((_familyname.concat('.')).concat(_givenname)).concat('+')).concat(result)).concat('@aussiepay.com');

        it ('Can delete a new hire ', function(){
            const login= new LoginPage()
            login.visit()
            login.fillEmail('mohammadarafat@epayroll.com')
            login.nextButton()
            login.fillPassword('abcde12345')
            login.sumbit()

            const onboard= new OnboardingPage()
            onboard.NavigateToMyEmployeeConfiguration()
            onboard.AddEmployee()
            onboard.ProvideEmployeeDetails(_familyname,_givenname,_email)
            onboard.SendInvitation()
            onboard.SearchEmployee(_email)
            onboard.DeleteNewHire()
            onboard.SearchEmployee(_email)

             //Assertion
            cy.xpath('//*[@id="tblEmployee"]/tbody/tr/td').should("contain.text",'No matching records found')
       
        })  

         it('Users can not proceed with onboarding link', () => {
            cy.mailosaurGetMessage(serverId, {
               sentTo: _email
            }).then(message => {
            var startonboarding = message.html.links[0].href;

            cy.visit(startonboarding)
            cy.title().should('be.equal', 'Welcome to Aussiepay')
           
           })
       })  

})  

   describe('Onboarding Test Cases : Resend invitation with a new email address', function () {

      var y = Math.floor((Math.random() * 11) + 1);
      var _familyname = familyname[y];
      var _givenname = givenname[y];
      var _email = ((((_familyname.concat('.')).concat(_givenname)).concat('+')).concat(result)).concat('@aussiepay.com');
      var email_edited = ((((_familyname.concat('.')).concat(_givenname)).concat('+')).concat(result)).concat('@epayroll.com')

      it ('Can resend a invitation changing email address ', function(){
      const login= new LoginPage()
         login.visit()
         login.fillEmail('mohammadarafat@epayroll.com')
         login.nextButton()
         login.fillPassword('abcde12345')
         login.sumbit()

         const onboard= new OnboardingPage()
         onboard.NavigateToMyEmployeeConfiguration()
         onboard.AddEmployee()
         onboard.ProvideEmployeeDetails(_familyname,_givenname,_email)
         onboard.SendInvitation()
         onboard.SearchEmployee(_email)
         onboard.EditEmail(email_edited)
         onboard.ResendInvitaion()
         onboard.SearchEmployee(email_edited)

          //Assertion
         cy.xpath('//*[@id="tblEmployee"]/tbody/tr/td').should("contain.text",(_familyname.concat(', ')).concat(_givenname) )

      })  

      it('Users can not proceed with old onboarding link', () => {
         cy.mailosaurGetMessage(serverId, {
            sentTo: _email
         }).then(message => {
         var startonboarding = message.html.links[0].href;

         cy.visit(startonboarding)
         
         //Assertion
         cy.title().should('be.equal', 'Welcome to Aussiepay')
      
         })
      })  

      it('Users can proceed with newly invited onboarding link', () => {
         cy.mailosaurGetMessage(serverId, {
            sentTo: email_edited
         }).then(message => {
         var startonboarding = message.html.links[0].href;

         cy.visit(startonboarding)
         cy.title().should('be.equal', 'Welcome New Hire')
      
         })
      }) 

})




