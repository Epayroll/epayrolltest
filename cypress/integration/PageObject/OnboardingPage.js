class OnboardingPage 
{
    NavigateToMyEmployeeConfiguration()
    {
        const button0=cy.xpath('//*[@id="nav"]/li[4]/a/p')
        button0.click()
        const button1=cy.xpath('//*[@id="nav"]/li[3]/a/p')
        button1.click()
        const button2=cy.xpath('//*[@id="nav"]/li[3]/ul/li[2]/a')
        button2.click()
        const button3=cy.xpath('//*[@id="nav"]/li[3]/ul/li[2]/ul/li[2]/a')
        button3.click()
    }

    AddEmployee()
    {
        const button0=cy.xpath('//*[@id="content"]/div/div[3]/div/div[2]/app-root/ng-component/ng-component/form/div/div/div[1]/div/div/span')
        button0.click()
    }

    ProvideEmployeeDetails(familyname,givenname,email)
    {
        const field1=cy.xpath('//*[@id="content"]/div/div[3]/div/div[2]/app-root/ng-component/ng-component/form/div[1]/div/div[2]/div[1]/div[1]/div/input')
        field1.clear()
        field1.type(familyname)

        const field2=cy.xpath('//*[@id="content"]/div/div[3]/div/div[2]/app-root/ng-component/ng-component/form/div[1]/div/div[2]/div[1]/div[2]/div/input')
        field2.clear()
        field2.type(givenname)

        const field3=cy.xpath('//*[@id="content"]/div/div[3]/div/div[2]/app-root/ng-component/ng-component/form/div[1]/div/div[2]/div[2]/div[1]/div/input')
        field3.clear()
        field3.type(email)

        cy.get('#content > div > div.row > div > div.col-md-12 > app-root > ng-component > ng-component > form > div:nth-child(1) > div > div.widget-content.row-border > div:nth-child(2) > div:nth-child(2) > div > select').select('190886')

        return this
    }
    SendInvitation()
    {
        const button0=cy.xpath('//*[@id="content"]/div/div[3]/div/div[2]/app-root/ng-component/ng-component/form/div[3]/div/button[2]')
        button0.click()
    }

    SearchEmployee(email)
    {
        const field1=cy.xpath('//*[@id="tblEmployee_filter"]/label/div/input')
        field1.clear()
        field1.type(email)
    }

    DeleteNewHire()
    {
        const button0=cy.xpath('//*[@id="tblEmployee"]/tbody/tr/td[6]/employee-actions/div/i')
        button0.click()
        const button1=cy.xpath('//*[@id="tblEmployee"]/tbody/tr/td[6]/employee-actions/div/ul/li[3]/a')
        button1.click()
        const button3=cy.xpath('/html/body/div[2]/div/div[2]/div/div/dx-button[2]/div/span')
        button3.click()
        cy.wait(2000)
        cy.reload()
    }
    ChangePassword()
    {
        const button0=cy.xpath('//button[contains(text(),"Continue")]')
        button0.click()

        const field2=cy.xpath('//input[@id="NewPassword"]')
        field2.clear()
        field2.type('abcde12345')

        const field3=cy.xpath('//input[@id="ConfirmPassword"]')
        field3.clear()
        field3.type('abcde12345')

        const button2=cy.xpath('/html/body/div/div/div[2]/button[2]')
        button2.click()

        cy.wait(3000)

    }

    NavigateToPersonalDetails()
    {
        cy.visit('https://saasuat.epayroll.com.au/Equip/Onboarding/Wizard/personaldetails')
    }

    ProvidePersonalDetails()
    {

  

        const field1=cy.xpath('//*[@id="content"]/div/div[3]/div/div[2]/app-root/ng-component/ng-component/form/div/div[1]/div[3]/div/div[3]/div[2]/div/input')
        field1.clear()
        field1.type('0426397119')

        const field2=cy.xpath('//*[@id="content"]/div/div[3]/div/div[2]/app-root/ng-component/ng-component/form/div/div[1]/div[3]/div/div[3]/div[3]/div/div/dx-date-box/div[1]/div/div[1]/input')
        field2.clear()
        field2.type('30/11/1993')

        cy.get('#content > div > div.row > div > div.col-md-10.col-md-offset-1 > app-root > ng-component > ng-component > form > div > div.widget.box > div.panel-body > div > div:nth-child(4) > div:nth-child(4) > div > select').select('Male')
   
        const field3=cy.xpath('//input[@formcontrolname="streetaddress1"]')
        field3.clear()
        field3.type('25 Bigge St')

        const field4=cy.xpath('//*[@id="content"]/div/div[3]/div/div[2]/app-root/ng-component/ng-component/form/div/div[1]/div[3]/div/div[5]/div[1]/div/input')
        field4.clear()
        field4.type('Liverpool')

        cy.get('#country').select('13')
        cy.get('#state').select('2')

        const field5=cy.xpath('//*[@id="content"]/div/div[3]/div/div[2]/app-root/ng-component/ng-component/form/div/div[1]/div[3]/div/div[6]/div[2]/div/input')
        field5.clear()
        field5.type('2170')

        const button1=cy.xpath('//button[contains(text(),"Next")]')
        button1.click()

    }

    ProvideEmmergencyDetails()
    {

        const field1=cy.xpath('//input[@formcontrolname="contactfamilyname"]')
        field1.clear()
        field1.type('Harshpreet')

        const field2=cy.xpath('//input[@formcontrolname="contactstreetaddress1"]')
        field2.clear()
        field2.type('25 Bigge Street Liverpool')

        const field3=cy.xpath('//input[@formcontrolname="contactrelationship"]')
        field3.clear()
        field3.type('Mom')

        const field4=cy.xpath('//input[@formcontrolname="contactmobile"]')
        field4.clear()
        field4.type('0426397119')

        const button2=cy.xpath('//button[contains(text(),"Next")]')
        button2.click()

    }


    EditEmail(email_edited)
    {
        const button = cy.xpath('//i[@class="glyphicon glyphicon-edit edit-icon"]')
        button.click()

        const field1=cy.xpath('//tbody/tr[1]/td[5]/div[1]/input[1]')
        field1.clear()
        field1.type(email_edited)

        const button2 = cy.xpath('//tbody/tr[1]/td[5]/div[1]/button[2]/span[1]')
        button2.click()

    }
    

    ResendInvitaion()
    {

        const button0=cy.xpath('//tbody/tr[1]/td[6]/employee-actions[1]/div[1]/i[1]')
        button0.click()

        const button3=cy.xpath('//*[@id="tblEmployee"]/tbody/tr/td[6]/employee-actions/div/ul/li[3]/a')
        button3.click()

        const button4=cy.xpath('//span[contains(text(),"Cancel")]')
        button4.click()

        cy.wait(3500)

        const button7=cy.xpath('//tbody/tr[1]/td[6]/employee-actions[1]/div[1]/i[1]')
        button7.click()

        const button1=cy.xpath('//tbody/tr[1]/td[6]/employee-actions[1]/div[1]/ul[1]/li[5]/a[1]') 
        button1.click()
        cy.reload()
    }


    NavigateToOnboardingWizard()

    {
        const button0=cy.xpath('//*[@id="nav"]/li[4]/a/p')
        button0.click()
        const button1=cy.xpath('//*[@id="nav"]/li[3]/a/p')
        button1.click()
        const button2=cy.xpath('//*[@id="nav"]/li[3]/ul/li[2]/a')
        button2.click()
        const button3=cy.xpath('//*[@id="nav"]/li[3]/ul/li[2]/ul/li[1]/a')
        button3.click()
    }

    SetNumberOfDaysForR(number)
    {
        const field1=cy.xpath('//*[@id="content"]/div/div[3]/div/div[2]/app-root/ng-component/ng-component/form/div/div[2]/div[12]/div[2]/div[1]/div[2]/dx-number-box/div/div[1]/input')
        field1.clear()
        field1.type(number)
    }

    ProvideTaxDetails()
    {
        const field5=cy.xpath('//input[@formcontrolname="TaxFileNumber"]')
        field5.clear()
        field5.type('555255242')

        const button2=cy.xpath('//input[@formcontrolname="IsDeclared"]')
        button2.click()

        const button1=cy.xpath('//button[contains(text(),"Next")]')
        button1.click()

        cy.wait(5000)

        const button7=cy.xpath('//button[contains(text(),"Next")]')
        button7.click()
    }

    ProvideBankDetails()
    {
        const field1=cy.xpath('//input[@formcontrolname="accountName"]')
        field1.clear()
        field1.type('Harshpreet')

        const field2=cy.xpath('//input[@formcontrolname="accountNumber"]')
        field2.clear()
        field2.type('12345678')

        const field3=cy.xpath('//input[@formcontrolname="bsbNumber"]')
        field3.clear()
        field3.type('062191')

        const field4=cy.xpath('//input[@formcontrolname="reference"]')
        field4.clear()
        field4.type('Salary')

        const button2=cy.xpath('//button[contains(text(),"Next")]')
        button2.click()  
    }

    Finishonboard()
    {
        
        const button2=cy.xpath('//input[@formcontrolname="isAgree"]')
        button2.click()

        const button1=cy.xpath('//button[contains(text(),"Read and Acknowledge")]')
        button1.click()

        cy.wait(5000)

        const button3=cy.xpath('//button[contains(text(),"Finish")]')
        button3.click()

    }

}

export default OnboardingPage