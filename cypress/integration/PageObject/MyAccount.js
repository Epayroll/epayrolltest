


class MyAccount
{
  
    NavigateToMyAccountHome()
    {
        const button1=cy.xpath('//*[@id="nav"]/li[4]/ul/li[2]/a')
        button1.click()
        const button2=cy.xpath('//*[@id="nav"]/li[4]/ul/li[2]/ul/li[1]/a')
        button2.click()
    }

    EditGeneralInformation()
    {
        const button1=cy.xpath('//button[@id="phMain_authorityView_ctl00_butEdit"]')
        button1.click()
       
    }

    RetypeGI(value)
    {
        const field=cy.xpath('//input[@id="phMain_ctl00_txtStreetAddress1"]')
        field.clear()
        field.type(value)
        return this
    }

    SaveGI()
    {
        const button1=cy.xpath('//button[@id="phMain_ctl00_btnsave"]')
        button1.click()
       
    }

    NavigateToChangePassword()
    {
        const button1=cy.xpath('//*[@id="nav"]/li[4]/ul/li[2]/a')
        button1.click()
        const button2=cy.xpath('//*[@id="nav"]/li[4]/ul/li[2]/ul/li[2]/a')
        button2.click()
    }

    TypeOldPassword(value)
    {
        const field=cy.xpath('//input[@id="OldPassword"]')
        field.clear()
        field.type(value)
        return this
    }

    TypeNewPassword(value)
    {
        const field1=cy.xpath('//input[@id="NewPassword"]')
        field1.clear()
        field1.type(value)

        const field2=cy.xpath('//input[@id="ConfirmPassword"]')
        field2.clear()
        field2.type(value)

        return this
    }

    SavePassword()
    {
        const button1=cy.xpath('//button[contains(text(),"Save Changes")]')
        button1.click()
    }

}

export default MyAccount