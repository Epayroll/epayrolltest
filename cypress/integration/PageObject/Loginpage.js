
class LoginPage
{
    visit()
    {
        cy.visit('https://saasuat.epayroll.com.au')
    }

    fillEmail(value)
    {
        const field=cy.get('#UserName')
        field.clear()
        field.type(value)
        return this
    }

    nextButton()
    {
        const button=cy.xpath('//button[@class="submit btn btn-primary pull-right"]')
        button.click()
    }


    fillPassword(value)
    {
        const field=cy.get('#Password')
        field.clear()
        field.type(value)
        return this
    }

    sumbit()
    {
        const button=cy.xpath('//button[@class="submit btn btn-primary pull-right"]')
        button.click()
    }

    forgetPassword()
    {
        const button=cy.xpath('//button[@name="forgot-password"]')
        button.click()
        const button1=cy.xpath('//*[@id="forgot-password-modal"]/div/div/div[3]/button[2]')
        button.click()
    }

}

export default LoginPage